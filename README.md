# Rust Web Example

This repository is dedicated to documenting my journey through *CS 410P/510 Rust Web Development* course. It serves as a personal space where I record my progress, challenges, and achievements as I delve deeper into the world of Rust for web development.

## Maintainer

- [Khalid Alasiri](kalasiri@pdx.edu)

## About the Repository

This repository contains a simple Q&A web application built using the Axum web framework in Rust. It demonstrates core concepts of RESTful API design and provides a foundation for building more complex web applications with Rust, featuring full persistence with a PostgreSQL database.

## Functionality

The application provides the following core CRUD (Create, Read, Update, Delete) operations for managing questions:

### Routes

* **GET `/questions`**
   - Lists all questions (with pagination).
   - **Query Parameters:**
      - `page` (optional): Page number for pagination (default: 1).
      - `page_size` (optional): Number of items per page (default: 10).

* **GET `/questions/:id`**
   - Retrieves a specific question by its ID.

* **POST `/questions`**
   - Creates a new question.
   - **Request Body (JSON):**
      ```json
      {
        "title": "Question title",
        "content": "Question details",
        "tags": ["tag1", "tag2"] // Optional
      }
      ```

* **PUT `/questions/:id`**
   - Updates an existing question by its ID.
   - **Request Body (JSON):** Same structure as POST.

* **DELETE `/questions/:id`**
   - Deletes a question by its ID.


### Data Persistence

- The application uses PostgreSQL for data storage. This ensures that all data remains persistent across server restarts. The data model is structured with the following fields in the `questions` table:
  - `id`: Primary Key, auto-incremented
  - `title`: String, not nullable
  - `content`: Text, not nullable
  - `tags`: Array of text, optional

## Database and Docker Setup

### Database Configuration

The application is backed by a PostgreSQL database, ensuring data persistence across sessions. The database is configured as follows:

- **Database Name:** `rustweb_db`
- **User:** `postgres`
- **Password:** Managed through Docker secrets **(store the password in db/password.txt)**

### Docker Environment

The application and database are containerized using Docker, facilitating easy setup and deployment. Here’s a brief overview of the Docker configuration:

- **`server` Service:** Runs the Rust application, configured to connect to the PostgreSQL service. This service handles the RESTful API and communicates with the database.
- **`frontend` Service:** Runs the Rust frontend application built with Yew, configured to connect to the backend API. This service serves the user interface and interacts with the backend service to fetch and display data.
- **`db` Service:** PostgreSQL database with persistent storage and health checks to ensure availability before the server starts.
- **Networks:** Both services communicate over a Docker-managed network.

### Running with Docker Compose

To start the application along with the PostgreSQL database, and frontend use Docker Compose:

```bash
docker-compose up --build
```

This setup will start the backend service on port 3000, the frontend service on port 8080, and the PostgreSQL database.

Access the Services:
- Frontend: http://localhost:8080
- Backend API: http://localhost:3000


## Frontend 

The application provides a user interface to interact with the backend Q&A API. Users can view questions, add new questions, update existing questions, and delete questions.

Also, there is a separate repo for the frontend with screenshots of the user interface here [Rust-Frontend](https://gitlab.cecs.pdx.edu/kalasiri/rust_frontend)