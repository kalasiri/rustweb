-- migrations/20240515052051_create_questions_table.sql
CREATE TABLE questions (
    id SERIAL PRIMARY KEY,
    title VARCHAR NOT NULL,
    content TEXT NOT NULL,
    tags TEXT[]
);