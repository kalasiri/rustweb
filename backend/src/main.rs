use axum::{
    extract::{Extension, Json, Path, Query},
    http::{self, Method, StatusCode},
    routing::{delete, get, post, put},
    Router,
};
use serde::{Deserialize, Serialize};
use sqlx::postgres::PgPool;
use std::net::SocketAddr;
extern crate thiserror;
use sqlx::Row;
use std::env::var;
use tower_http::cors::{Any, CorsLayer};

/// Creates a database connection pool and runs migrations.
///
/// # Errors
///
/// Returns an error if the database connection cannot be established
/// or if migrations fail to run.
///
/// # Examples
///
/// ```
/// let pool = create_db_pool().await?;
/// ```
async fn create_db_pool() -> Result<PgPool, Box<dyn std::error::Error>> {
    let pwf = var("PG_PASSWORDFILE").map_err(|_| "POSTGRES_PASSWORD must be set")?;
    let password = std::fs::read_to_string(pwf).map_err(|_| "Could not read password file")?;
    let database_url = format!(
        "postgres://{}:{}@{}:5432/{}",
        var("PG_USER")?,
        password.trim(),
        var("PG_HOST")?,
        var("PG_DBNAME")?,
    );
    let pool = PgPool::connect(&database_url).await?;
    match sqlx::migrate!().run(&pool).await {
        Ok(_) => {
            println!("Migrations ran successfully");
            let count: (i64,) = sqlx::query_as("SELECT COUNT(*) FROM questions")
                .fetch_one(&pool)
                .await?;
            println!("Questions count: {}", count.0);
        }
        Err(e) => eprintln!("Failed to run migrations: {}", e),
    }
    Ok(pool)
}

/// Represents a question with its properties.
#[derive(Clone, Debug, Deserialize, Serialize)]
struct Question {
    #[serde(skip_deserializing)]
    id: u32,
    title: String,
    content: String,
    tags: Option<Vec<String>>,
}

/// Represents the pagination parameters for listing questions.
#[derive(Deserialize, Default)]
struct PaginationParams {
    page: Option<usize>,
    page_size: Option<usize>,
}

/// Initializes the questions table with default values if empty.
///
/// # Errors
///
/// Returns an error if the database query fails.
///
/// # Examples
///
/// ```
/// initialize_questions_if_empty(&pool).await?;
/// ```
async fn initialize_questions_if_empty(pool: &PgPool) -> Result<(), sqlx::Error> {
    let count: (i64,) = sqlx::query_as("SELECT COUNT(*) FROM questions")
        .fetch_one(pool)
        .await?;

    if count.0 == 0 {
        sqlx::query("INSERT INTO questions (title, content, tags) VALUES ($1, $2, $3)")
            .bind("What is Rust?")
            .bind(
                "Rust is a systems programming language focused on safety, speed, and concurrency.",
            )
            .bind(vec!["programming", "rust", "systems"])
            .execute(pool)
            .await?;
    }
    Ok(())
}

/// Handles the GET request to list all questions.
///
/// # Parameters
///
/// - `pool`: The database connection pool.
/// - `pagination_params`: The pagination parameters.
///
/// # Returns
///
/// A JSON response containing a list of questions or an internal server error status.
async fn list_questions(
    Extension(pool): Extension<PgPool>,
    Query(pagination_params): Query<PaginationParams>,
) -> Result<Json<Vec<Question>>, StatusCode> {
    let questions = sqlx::query(
        "SELECT id, title, content, tags FROM questions ORDER BY id LIMIT $1 OFFSET $2",
    )
    .bind(pagination_params.page_size.unwrap_or(10) as i32)
    .bind(
        ((pagination_params.page.unwrap_or(1) - 1) * pagination_params.page_size.unwrap_or(10))
            as i32,
    )
    .fetch_all(&pool)
    .await
    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let mut results = Vec::new();
    for question in questions {
        results.push(Question {
            id: question
                .try_get::<i32, _>("id")
                .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)? as u32,
            title: question.get("title"),
            content: question.get("content"),
            tags: question.get("tags"),
        });
    }

    Ok(Json(results))
}

/// Handles the GET request to get a question by its ID.
///
/// # Parameters
///
/// - `pool`: The database connection pool.
/// - `question_id`: The ID of the question to retrieve.
///
/// # Returns
///
/// A JSON response containing the question or a not found status.
async fn get_question(
    Extension(pool): Extension<PgPool>,
    Path(question_id): Path<u32>,
) -> Result<Json<Question>, StatusCode> {
    let result = sqlx::query("SELECT id, title, content, tags FROM questions WHERE id = $1")
        .bind(question_id as i32)
        .fetch_one(&pool)
        .await;

    match result {
        Ok(row) => {
            let question = Question {
                id: row
                    .try_get::<i32, _>("id")
                    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)? as u32,
                title: row
                    .try_get("title")
                    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?,
                content: row
                    .try_get("content")
                    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?,
                tags: row
                    .try_get("tags")
                    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?,
            };
            Ok(Json(question))
        }
        Err(sqlx::Error::RowNotFound) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

/// Handles the POST request to create a new question.
///
/// # Parameters
///
/// - `pool`: The database connection pool.
/// - `new_question`: The new question to create.
///
/// # Returns
///
/// A JSON response containing the created question or an internal server error status.
async fn create_question(
    Extension(pool): Extension<PgPool>,
    Json(new_question): Json<Question>,
) -> Result<Json<Question>, StatusCode> {
    let result =
        sqlx::query("INSERT INTO questions (title, content, tags) VALUES ($1, $2, $3) RETURNING *")
            .bind(&new_question.title)
            .bind(&new_question.content)
            .bind(&new_question.tags)
            .fetch_one(&pool)
            .await;

    match result {
        Ok(row) => {
            let question = Question {
                id: row
                    .try_get::<i32, _>("id")
                    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)? as u32,
                title: row.get("title"),
                content: row.get("content"),
                tags: row.get("tags"),
            };
            Ok(Json(question))
        }
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

/// Handles the PUT request to update a question.
///
/// # Parameters
///
/// - `pool`: The database connection pool.
/// - `question_id`: The ID of the question to update.
/// - `updated_question`: The updated question data.
///
/// # Returns
///
/// A JSON response containing the updated question or a not found status.
async fn update_question(
    Extension(pool): Extension<PgPool>,
    Path(question_id): Path<u32>,
    Json(updated_question): Json<Question>,
) -> Result<Json<Question>, StatusCode> {
    let result = sqlx::query(
        "UPDATE questions SET title = $1, content = $2, tags = $3 WHERE id = $4 RETURNING id, title, content, tags"
    )
    .bind(&updated_question.title)
    .bind(&updated_question.content)
    .bind(&updated_question.tags)
    .bind(question_id as i32) // Convert question_id to i32
    .fetch_one(&pool)
    .await;

    match result {
        Ok(row) => {
            let question = Question {
                id: row
                    .try_get::<i32, _>("id")
                    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)? as u32,
                title: row.get("title"),
                content: row.get("content"),
                tags: row.get("tags"),
            };
            Ok(Json(question))
        }
        Err(sqlx::Error::RowNotFound) => Err(StatusCode::NOT_FOUND),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

/// Handles the DELETE request to delete a question.
///
/// # Parameters
///
/// - `pool`: The database connection pool.
/// - `question_id`: The ID of the question to delete.
///
/// # Returns
///
/// A no content status if the deletion was successful or a not found status.
async fn delete_question(
    Extension(pool): Extension<PgPool>,
    Path(question_id): Path<u32>,
) -> Result<StatusCode, StatusCode> {
    let result = sqlx::query("DELETE FROM questions WHERE id = $1")
        .bind(question_id as i32)
        .execute(&pool)
        .await;

    match result {
        Ok(_) => Ok(StatusCode::NO_CONTENT),
        Err(_) => Err(StatusCode::NOT_FOUND),
    }
}

/// The main function that starts the application.
///
/// # Panics
///
/// Panics if the database connection pool cannot be created or if the
/// application fails to start.
#[tokio::main]
async fn main() {
    let pool = create_db_pool()
        .await
        .expect("Failed to create the database pool");

    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST, Method::PUT, Method::DELETE])
        .allow_origin(Any)
        .allow_headers([http::header::CONTENT_TYPE]);

    initialize_questions_if_empty(&pool).await.unwrap();
    let app = Router::new()
        .route("/questions", get(list_questions))
        .route("/questions", post(create_question))
        .route("/questions/:question_id", put(update_question))
        .route("/questions/:question_id", delete(delete_question))
        .route("/questions/:question_id", get(get_question))
        .layer(Extension(pool))
        .layer(cors);

    let ip = SocketAddr::new([0, 0, 0, 0].into(), 3000);
    println!("Application started on port 3000");
    let listener = tokio::net::TcpListener::bind(ip).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}
