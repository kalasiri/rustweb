use crate::app::App;
use wasm_bindgen::prelude::*;
use yew::prelude::*;

mod app;

#[function_component(MainApp)]
fn main_app() -> Html {
    html! { <App /> }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    yew::Renderer::<MainApp>::new().render();
}
