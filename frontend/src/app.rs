use gloo::storage::{LocalStorage, Storage};
use log::info;
use reqwasm::http::Request;
use serde::{Deserialize, Serialize};
use wasm_bindgen_futures::spawn_local;
use yew::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct Question {
    id: u32,
    title: String,
    content: String,
    tags: Option<Vec<String>>,
}

pub struct App {
    questions: Vec<Question>,
    loading: bool,
    error: Option<String>,
    base_api_url: String,
    new_question: Question,
    edit_question: Option<Question>,
    view: View,
}

pub enum Msg {
    FetchQuestions,
    Success(Vec<Question>),
    Error(String),
    ShowAddForm,
    ShowEditForm(Question),
    ShowList,
    UpdateNewQuestionTitle(String),
    UpdateNewQuestionContent(String),
    UpdateNewQuestionTags(String),
    SubmitNewQuestion,
    SubmitEditedQuestion,
    QuestionAdded(Question),
    QuestionUpdated(Question),
    DeleteQuestion(u32),
    QuestionDeleted(u32),
}

#[derive(PartialEq)]
enum View {
    List,
    AddForm,
    EditForm,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let base_api_url = LocalStorage::get("BASE_API_URL")
            .unwrap_or_else(|_| "http://localhost:3000".to_string());
        ctx.link().send_message(Msg::FetchQuestions);
        Self {
            questions: vec![],
            loading: true,
            error: None,
            base_api_url,
            new_question: Question::default(),
            edit_question: None,
            view: View::List,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::FetchQuestions => {
                let link = ctx.link().clone();
                let api_url = self.base_api_url.clone();
                spawn_local(async move {
                    info!("Fetching questions...");
                    let result = Request::get(&format!("{}/questions", api_url)).send().await;
                    match result {
                        Ok(response) => {
                            if response.ok() {
                                info!("Fetch successful");
                                let questions: Result<Vec<Question>, _> = response.json().await;
                                match questions {
                                    Ok(questions) => link.send_message(Msg::Success(questions)),
                                    Err(_) => {
                                        let error_msg = "Failed to parse response".into();
                                        info!("{}", error_msg);
                                        link.send_message(Msg::Error(error_msg))
                                    }
                                }
                            } else {
                                let error_msg = "Failed to fetch questions".into();
                                info!("{}", error_msg);
                                link.send_message(Msg::Error(error_msg));
                            }
                        }
                        Err(err) => {
                            let error_msg = format!("Failed to fetch questions: {}", err);
                            info!("{}", error_msg);
                            link.send_message(Msg::Error(error_msg));
                        }
                    }
                });
                false
            }
            Msg::Success(questions) => {
                self.questions = questions;
                self.loading = false;
                true
            }
            Msg::Error(error) => {
                self.error = Some(error);
                self.loading = false;
                true
            }
            Msg::ShowAddForm => {
                self.new_question = Question::default();
                self.view = View::AddForm;
                true
            }
            Msg::ShowEditForm(question) => {
                self.edit_question = Some(question);
                self.view = View::EditForm;
                true
            }
            Msg::ShowList => {
                self.view = View::List;
                true
            }
            Msg::UpdateNewQuestionTitle(title) => {
                if self.view == View::AddForm {
                    self.new_question.title = title;
                } else if let Some(edit_question) = &mut self.edit_question {
                    edit_question.title = title;
                }
                true
            }
            Msg::UpdateNewQuestionContent(content) => {
                if self.view == View::AddForm {
                    self.new_question.content = content;
                } else if let Some(edit_question) = &mut self.edit_question {
                    edit_question.content = content;
                }
                true
            }
            Msg::UpdateNewQuestionTags(tags) => {
                if self.view == View::AddForm {
                    self.new_question.tags =
                        Some(tags.split(',').map(|tag| tag.trim().to_string()).collect());
                } else if let Some(edit_question) = &mut self.edit_question {
                    edit_question.tags =
                        Some(tags.split(',').map(|tag| tag.trim().to_string()).collect());
                }
                true
            }
            Msg::SubmitNewQuestion => {
                let new_question = self.new_question.clone();
                let link = ctx.link().clone();
                let api_url = self.base_api_url.clone();
                spawn_local(async move {
                    let result = Request::post(&format!("{}/questions", api_url))
                        .header("Content-Type", "application/json")
                        .body(serde_json::to_string(&new_question).unwrap())
                        .send()
                        .await;
                    match result {
                        Ok(response) => {
                            if response.ok() {
                                let question: Result<Question, _> = response.json().await;
                                match question {
                                    Ok(question) => link.send_message(Msg::QuestionAdded(question)),
                                    Err(_) => link.send_message(Msg::Error(
                                        "Failed to parse response".into(),
                                    )),
                                }
                            } else {
                                link.send_message(Msg::Error("Failed to add question".into()));
                            }
                        }
                        Err(err) => link
                            .send_message(Msg::Error(format!("Failed to add question: {}", err))),
                    }
                });
                false
            }
            Msg::SubmitEditedQuestion => {
                if let Some(edit_question) = self.edit_question.clone() {
                    let link = ctx.link().clone();
                    let api_url = self.base_api_url.clone();
                    spawn_local(async move {
                        let result =
                            Request::put(&format!("{}/questions/{}", api_url, edit_question.id))
                                .header("Content-Type", "application/json")
                                .body(serde_json::to_string(&edit_question).unwrap())
                                .send()
                                .await;
                        match result {
                            Ok(response) => {
                                if response.ok() {
                                    link.send_message(Msg::QuestionUpdated(edit_question));
                                } else {
                                    link.send_message(Msg::Error(
                                        "Failed to update question".into(),
                                    ));
                                }
                            }
                            Err(err) => link.send_message(Msg::Error(format!(
                                "Failed to update question: {}",
                                err
                            ))),
                        }
                    });
                }
                false
            }
            Msg::QuestionAdded(question) => {
                self.questions.push(question);
                self.view = View::List;
                true
            }
            Msg::QuestionUpdated(updated_question) => {
                if let Some(pos) = self
                    .questions
                    .iter()
                    .position(|q| q.id == updated_question.id)
                {
                    self.questions[pos] = updated_question;
                }
                self.view = View::List;
                true
            }
            Msg::DeleteQuestion(question_id) => {
                let link = ctx.link().clone();
                let api_url = self.base_api_url.clone();
                spawn_local(async move {
                    let result = Request::delete(&format!("{}/questions/{}", api_url, question_id))
                        .send()
                        .await;
                    match result {
                        Ok(response) => {
                            if response.ok() {
                                link.send_message(Msg::QuestionDeleted(question_id));
                            } else {
                                link.send_message(Msg::Error("Failed to delete question".into()));
                            }
                        }
                        Err(err) => link.send_message(Msg::Error(format!(
                            "Failed to delete question: {}",
                            err
                        ))),
                    }
                });
                false
            }
            Msg::QuestionDeleted(question_id) => {
                self.questions.retain(|q| q.id != question_id);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div>
                <h1>{ "Questions" }</h1>
                { self.view_content(ctx) }
            </div>
        }
    }
}

impl App {
    fn view_content(&self, ctx: &Context<Self>) -> Html {
        match self.view {
            View::List => self.view_list(ctx),
            View::AddForm => self.view_add_form(ctx),
            View::EditForm => self.view_edit_form(ctx),
        }
    }

    fn view_list(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div>
                <button type="primary" onclick={ctx.link().callback(|_| Msg::ShowAddForm)}>{ "Add New Question" }</button>
                { if self.loading {
                    html! { <p>{ "Loading..." }</p> }
                } else if let Some(ref error) = self.error {
                    html! { <p>{ error }</p> }
                } else {
                    html! {
                        <ul>
                            { for self.questions.iter().map(|question| self.view_question(question.clone(), ctx)) }
                        </ul>
                    }
                }}
            </div>
        }
    }

    fn view_add_form(&self, ctx: &Context<Self>) -> Html {
        let question = self.new_question.clone();
        html! {
            <div class="form-container">
                <h2>{ "New Question" }</h2>
                <form onsubmit={ctx.link().callback(|e: SubmitEvent| {
                    e.prevent_default();
                    Msg::SubmitNewQuestion
                })}>
                    <label for="title">{ "Title:" }</label>
                    <input type="text" id="title" name="title"
                           value={question.title.clone()}
                           oninput={ctx.link().callback(|e: InputEvent| {
                                let input: web_sys::HtmlInputElement = e.target_unchecked_into();
                                Msg::UpdateNewQuestionTitle(input.value())
                           })} /><br/><br/>
                    <label for="content">{ "Content:" }</label>
                    <textarea id="content" name="content"
                              value={question.content.clone()}
                              oninput={ctx.link().callback(|e: InputEvent| {
                                let input: web_sys::HtmlTextAreaElement = e.target_unchecked_into();
                                Msg::UpdateNewQuestionContent(input.value())
                           })}></textarea><br/><br/>
                    <label for="tags">{ "Tags (comma separated):" }</label>
                    <input type="text" id="tags" name="tags"
                           value={question.tags.clone().unwrap_or_default().join(", ")}
                           oninput={ctx.link().callback(|e: InputEvent| {
                                let input: web_sys::HtmlInputElement = e.target_unchecked_into();
                                Msg::UpdateNewQuestionTags(input.value())
                           })} /><br/><br/>
                    <button type="submit">{ "Submit" }</button>
                    <button type="danger" onclick={ctx.link().callback(|_| Msg::ShowList)}>{ "Cancel" }</button>
                </form>
            </div>
        }
    }

    fn view_edit_form(&self, ctx: &Context<Self>) -> Html {
        if let Some(edit_question) = self.edit_question.clone() {
            html! {
                <div class="form-container">
                    <h2>{ "Edit Question" }</h2>
                    <form onsubmit={ctx.link().callback(|e: SubmitEvent| {
                        e.prevent_default();
                        Msg::SubmitEditedQuestion
                    })}>
                        <label for="title">{ "Title:" }</label>
                        <input type="text" id="title" name="title"
                               value={edit_question.title.clone()}
                               oninput={ctx.link().callback(|e: InputEvent| {
                                    let input: web_sys::HtmlInputElement = e.target_unchecked_into();
                                    Msg::UpdateNewQuestionTitle(input.value())
                               })} /><br/><br/>
                        <label for="content">{ "Content:" }</label>
                        <textarea id="content" name="content"
                                  value={edit_question.content.clone()}
                                  oninput={ctx.link().callback(|e: InputEvent| {
                                    let input: web_sys::HtmlTextAreaElement = e.target_unchecked_into();
                                    Msg::UpdateNewQuestionContent(input.value())
                               })}></textarea><br/><br/>
                        <label for="tags">{ "Tags (comma separated):" }</label>
                        <input type="text" id="tags" name="tags"
                               value={edit_question.tags.clone().unwrap_or_default().join(", ")}
                               oninput={ctx.link().callback(|e: InputEvent| {
                                    let input: web_sys::HtmlInputElement = e.target_unchecked_into();
                                    Msg::UpdateNewQuestionTags(input.value())
                               })} /><br/><br/>
                        <button type="submit">{ "Submit" }</button>
                        <button type="danger" onclick={ctx.link().callback(|_| Msg::ShowList)}>{ "Cancel" }</button>
                    </form>
                </div>
            }
        } else {
            html! {}
        }
    }

    fn view_question(&self, question: Question, ctx: &Context<Self>) -> Html {
        let question_clone = question.clone();
        html! {
            <li>
                <div class="question-card">
                    <h2>{ &question.title }</h2>
                    <p>{ &question.content }</p>
                    {
                        if let Some(tags) = &question.tags {
                            html! { <p>{ format!("Tags: {:?}", tags) }</p> }
                        } else {
                            html! {}
                        }
                    }
                    <button type="primary" onclick={ctx.link().callback(move |_| Msg::ShowEditForm(question_clone.clone()))}>{ "Edit" }</button>
                    <button type="danger" onclick={ctx.link().callback(move |_| Msg::DeleteQuestion(question.id))}>{ "Delete" }</button>
                </div>
            </li>
        }
    }
}
